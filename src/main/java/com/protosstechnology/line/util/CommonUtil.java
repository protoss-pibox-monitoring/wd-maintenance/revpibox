package com.protosstechnology.line.util;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import com.protosstechnology.line.service.impl.LicenceServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommonUtil {

    private  CommonUtil(){

    }

    public static String cleansingMessage(StringBuffer outBuffer) {

        String data = outBuffer.toString();
        try {
            StringBuffer tempBuffer = new StringBuffer();
            int incrementor = 0;
            int dataLength = data.length();
            while (incrementor < dataLength) {
                char charecterAt = data.charAt(incrementor);
                if (charecterAt == '%') {
                    tempBuffer.append("<percentage>");
                } else if (charecterAt == '+') {
                    tempBuffer.append("<plus>");
                } else {
                    tempBuffer.append(charecterAt);
                }
                incrementor++;
            }
            data = tempBuffer.toString();
            data = URLDecoder.decode(data, "utf-8");
            data = data.replaceAll("<percentage>", "%");
            data = data.replaceAll("<plus>", "+");
        } catch(Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public static Timestamp getCurrentDateTimestamp() {
        Timestamp today = null;
        try {
            Date nowDate = Calendar.getInstance().getTime();
            today = new java.sql.Timestamp(nowDate.getTime());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return today;
    }
    
    public static Map<String, String> getMapParameter(HttpServletRequest request){
    	Map<String, String> mapContext = new HashMap<>();
        Map<String, String[]> paramerers = request.getParameterMap();
        for (Object key : paramerers.keySet()) {
             String value = paramerers.get(key)[0];
             mapContext.put(String.valueOf(key), value);
        }
        
        return mapContext;

    }
    

    public static String getDateTime() {
    	Date now = new Date();

    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

    	simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
    	
    	return simpleDateFormat.format(now);
    }

    

    public static String getHour() {
    	Date now = new Date();

    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH");

    	simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
    	
    	return simpleDateFormat.format(now);
    }
    
    public static Map<String, String> getToken(){
    	Map<String, String> map = new HashMap<>();
    	StringBuffer sb = new StringBuffer();
    	//Enumeration<NetworkInterface> networkInterfaces;
    	List<NetworkInterface> networkInterfaces ;
		try {
			networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			networkInterfaces.sort((s1, s2) -> s1.getName().compareTo(s2.getName())); 
			
			for (NetworkInterface ni : networkInterfaces) {
				byte[] hardwareAddress = ni.getHardwareAddress();
			    if (hardwareAddress != null) {
			        String[] hexadecimalFormat = new String[hardwareAddress.length];
			        for (int i = 0; i < hardwareAddress.length; i++) {
			            hexadecimalFormat[i] = String.format("%02X", hardwareAddress[i]).toLowerCase();
			        }
			        sb.append(String.join(":", hexadecimalFormat));
			        break;
			    }
			}
			
			//Encode Base64
			String token = new String(Base64.getEncoder().encode(sb.toString().getBytes()));
			map.put("token", token);
		} catch (SocketException e) {
			map.put("token", "inactive");
		}
		
        
        return map;

    }
    
    public static Map<String, String> getTokenFull(){
    	Map<String, String> map = new HashMap<>();
    	StringBuffer sb = new StringBuffer();
//    	Enumeration<NetworkInterface> networkInterfaces;
    	List<NetworkInterface> networkInterfaces ;
		try {
			networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			networkInterfaces.sort((s1, s2) -> s1.getName().compareTo(s2.getName())); 
			
			for (NetworkInterface ni : networkInterfaces) {
				byte[] hardwareAddress = ni.getHardwareAddress();
			    if (hardwareAddress != null) {
			        String[] hexadecimalFormat = new String[hardwareAddress.length];
			        for (int i = 0; i < hardwareAddress.length; i++) {
			            hexadecimalFormat[i] = String.format("%02X", hardwareAddress[i]).toLowerCase();
			        }
			        sb.append(String.join(":", hexadecimalFormat));
			        break;
			    }
			}
			
			//Encode Base64
			String token = new String(Base64.getEncoder().encode(sb.toString().getBytes()));
			map.put("token", token);
			map.put("macAddress", sb.toString());
		} catch (SocketException e) {
			map.put("token", "inactive");
		}
		
        
        return map;

    }
}
