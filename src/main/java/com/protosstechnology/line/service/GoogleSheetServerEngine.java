package com.protosstechnology.line.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class GoogleSheetServerEngine{

    @Value("${google.api.url}")
    protected String googleApiUrl = "https://script.google.com/macros/s";

    public ResponseEntity<String> list(String mapUrl) {
        ResponseEntity<String> reponseEntity =null;                                           
        try{

            RestTemplate restTemplate = new RestTemplate();
            String url = this.googleApiUrl+mapUrl;
            reponseEntity = restTemplate.exchange(url,HttpMethod.GET,HttpEntity.EMPTY, String.class);
            restTemplate = null;
            return reponseEntity;

        }catch(Exception e){
            e.printStackTrace();
            return reponseEntity;

        }
        
    }
    
    public ResponseEntity<String> find(String mapUrl,String queryJson) {
        ResponseEntity<String> reponseEntity =null;                                           
        try{

            RestTemplate restTemplate = new RestTemplate();
            String url = this.googleApiUrl+mapUrl+"&query={query}";
            Map<String,Object> uriVar = new HashMap();
            uriVar.put("query", queryJson);
            reponseEntity = restTemplate.exchange(url,HttpMethod.GET,HttpEntity.EMPTY, String.class,uriVar);
            restTemplate = null;
            return reponseEntity;

        }catch(Exception e){
            e.printStackTrace();
            return reponseEntity;

        }
        
    }
    
    public ResponseEntity<String> add(String mapUrl,String dataJson) {
        ResponseEntity<String> reponseEntity =null;                                           
        try{

            RestTemplate restTemplate = new RestTemplate();
            String url = this.googleApiUrl+mapUrl;
            
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<String> entity = new HttpEntity<String>(dataJson,headers);
            
            restTemplate.exchange(url,HttpMethod.POST,entity, String.class);
            
            url = this.googleApiUrl+mapUrl+"&query={query}";
            Map<String,Object> uriVar = new HashMap();
            uriVar.put("query", dataJson);
            
            reponseEntity = restTemplate.exchange(url,HttpMethod.GET,HttpEntity.EMPTY, String.class,uriVar);
            restTemplate = null;
            return reponseEntity;

        }catch(Exception e){
            e.printStackTrace();
            return reponseEntity;

        }
        
    }
    
    public ResponseEntity<String> update (String mapUrl,String idRecord,String dataJson) {
        ResponseEntity<String> reponseEntity =null;                                           
        try{

            RestTemplate restTemplate = new RestTemplate();
            String url = this.googleApiUrl+mapUrl+"/"+idRecord+"&method=PUT";
            
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<String> entity = new HttpEntity<String>(dataJson,headers);
            
            restTemplate.exchange(url,HttpMethod.POST,entity, String.class);
            
            url = this.googleApiUrl+mapUrl+"&query={query}";
            Map<String,Object> uriVar = new HashMap();
            uriVar.put("query", dataJson);
            
            reponseEntity = restTemplate.exchange(url,HttpMethod.GET,HttpEntity.EMPTY, String.class,uriVar);
            restTemplate = null;
            return reponseEntity;

        }catch(Exception e){
            e.printStackTrace();
            return reponseEntity;

        }
        
    }

}