package com.protosstechnology.line.service;

import java.util.Map;

import com.protosstechnology.line.model.ResponseModel;

public interface LicenceService{

    ResponseModel list(Map mapParam,ResponseModel responseModel);
    ResponseModel find(Map mapParam,ResponseModel responseModel);
    ResponseModel add(Map mapParam,ResponseModel responseModel);

}