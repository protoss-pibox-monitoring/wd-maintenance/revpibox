package com.protosstechnology.line.service;

import java.util.Map;

import com.protosstechnology.line.model.ResponseModel;

public interface MachineService{

    ResponseModel list(Map mapParam,ResponseModel responseModel);
    ResponseModel find(Map mapParam,ResponseModel responseModel);
    ResponseModel add(Map mapParam,ResponseModel responseModel);
    ResponseModel update(Map mapParam,String idRecord,ResponseModel responseModel);

}