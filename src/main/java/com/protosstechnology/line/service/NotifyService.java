package com.protosstechnology.line.service;

import java.util.Map;

import com.protosstechnology.line.model.ResponseModel;

public interface NotifyService{

    ResponseModel notifyLineMessage(Map mapParam,ResponseModel result);
    ResponseModel notifyLineMessage(String message,String lineNotifToken,ResponseModel result);

}