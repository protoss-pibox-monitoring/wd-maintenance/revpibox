package com.protosstechnology.line.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.protosstechnology.line.util.CommonUtil;

public abstract class LineNotifyServerEngine{

    @Value("${line.url}")
    protected String lineServer = "https://notify-api.line.me/api/notify";

    public ResponseEntity<String> sendLineNotify(String message, 
                                                 String token) {
        ResponseEntity<String> reponseEntity =null;                                           
        try{

            RestTemplate restTemplate = new RestTemplate();
            String url = this.lineServer;
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Content-Type", "application/x-www-form-urlencoded");
            headers.add("Authorization", "Bearer "+token);
            StringBuffer sb = new StringBuffer(message);
            message = CommonUtil.cleansingMessage(sb);
            
            MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            body.add("message", message);
        
            HttpEntity<Object> entity = new HttpEntity<>(body, headers);
            
            reponseEntity = restTemplate.exchange(url,HttpMethod.POST,entity, String.class);
            restTemplate = null;
            return reponseEntity;

        }catch(Exception e){
            e.printStackTrace();
            return reponseEntity;

        }
        
    }

}