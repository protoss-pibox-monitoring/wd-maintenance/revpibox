package com.protosstechnology.line.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.protosstechnology.commons.util.JSONUtil;
import com.protosstechnology.line.ApplicationConstant;
import com.protosstechnology.line.exception.LineException;
import com.protosstechnology.line.model.ResponseModel;
import com.protosstechnology.line.service.ActivityService;
import com.protosstechnology.line.service.GoogleSheetServerEngine;
import com.protosstechnology.line.service.MachineService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MachineServiceImpl extends GoogleSheetServerEngine implements MachineService {

	@Value("${google.machine.url}")
    private String url;

    @Override
    public ResponseModel list(Map mapParam, ResponseModel responseModel) {

        try {
            ResponseEntity<String> result = list(url);

            if (result == null)
                throw new LineException("===- Can't Send Line Notify  -===");

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
            responseModel.setError_code(null);
            responseModel.setMessage("List Data Successful");
            responseModel.setData(JSONUtil.mapFromJson(result.getBody()));
            return responseModel;

        } catch (Exception e) {
            /* sent object response for exception case */
            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1001);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
    }

	@Override
	public ResponseModel find(Map mapParam, ResponseModel responseModel) {
		// TODO Auto-generated method stub
		try {
            ResponseEntity<String> result = find(url,JSONUtil.getJSONSerializer().serialize(mapParam));

            if (result == null)
                throw new LineException("===- Can't Send Line Notify  -===");

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
            responseModel.setError_code(null);
            responseModel.setMessage("List Data Successful");
            responseModel.setData(JSONUtil.mapFromJson(result.getBody()));
            return responseModel;

        } catch (Exception e) {
            /* sent object response for exception case */
            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1001);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
	}

	@Override
	public ResponseModel add(Map mapParam, ResponseModel responseModel) {
		try {
            ResponseEntity<String> result = add(url,JSONUtil.getJSONSerializer().serialize(mapParam));

            if (result == null)
                throw new LineException("===- Can't Send Line Notify  -===");

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
            responseModel.setError_code(null);
            responseModel.setMessage("Add Data Successful");
            responseModel.setData(JSONUtil.mapFromJson(result.getBody()));
            return responseModel;

        } catch (Exception e) {
            /* sent object response for exception case */
            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1001);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
	}

	@Override
	public ResponseModel update(Map mapParam,String idRecord, ResponseModel responseModel) {
		try {
            ResponseEntity<String> result = update(url,idRecord,JSONUtil.getJSONSerializer().serialize(mapParam));

            if (result == null)
                throw new LineException("===- Can't Send Line Notify  -===");

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
            responseModel.setError_code(null);
            responseModel.setMessage("Add Data Successful");
            responseModel.setData(JSONUtil.mapFromJson(result.getBody()));
            return responseModel;

        } catch (Exception e) {
            /* sent object response for exception case */
            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1001);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
	}

    
}