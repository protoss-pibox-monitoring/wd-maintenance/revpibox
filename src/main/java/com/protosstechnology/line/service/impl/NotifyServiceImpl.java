package com.protosstechnology.line.service.impl;

import java.util.Map;

import org.apache.velocity.Template;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.protosstechnology.line.ApplicationConstant;
import com.protosstechnology.line.exception.LineException;
import com.protosstechnology.line.model.ResponseModel;
import com.protosstechnology.line.service.LineNotifyServerEngine;
import com.protosstechnology.line.service.NotifyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotifyServiceImpl extends LineNotifyServerEngine implements NotifyService {

	//@Value("${line.notif.token}")
    //private String lineNotifToken;

    @Override
    public ResponseModel notifyLineMessage(Map mapParam, ResponseModel responseModel) {

        try {
            String messageSend = String.valueOf(mapParam.get("message"));
            String lineNotifToken = String.valueOf(mapParam.get("lineNotifToken"));
            /* send line notify */
           
            ResponseEntity<String> result = sendLineNotify(messageSend, lineNotifToken);

            if (result == null)
                throw new LineException("===- Can't Send Line Notify  -===");

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
            responseModel.setError_code(null);
            responseModel.setMessage("Send notify case default successful.");
            return responseModel;

        } catch (Exception e) {
            /* sent object response for exception case */
            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1001);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
    }

	@Override
	public ResponseModel notifyLineMessage(String message,String lineNotifToken, ResponseModel responseModel) {
		try {
            /* send line notify */
           
            ResponseEntity<String> result = sendLineNotify(message, lineNotifToken);

            if (result == null)
                throw new LineException("===- Can't Send Line Notify  -===");

            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_SUCCESS);
            responseModel.setError_code(null);
            responseModel.setMessage("Send notify case default successful.");
            return responseModel;

        } catch (Exception e) {
            /* sent object response for exception case */
            responseModel.setSuccess(ApplicationConstant.RESPONSE_STATUS_ERROR);
            responseModel.setError_code(ApplicationConstant.ERROR_CODE_1001);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
            return responseModel;

        }
	}

    
}