package com.protosstechnology.line.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.protosstechnology.line.ApplicationConstant;
import com.protosstechnology.line.model.ResponseModel;
import com.protosstechnology.line.service.NotifyService;
import com.protosstechnology.line.util.CommonUtil;

@RestController
@RequestMapping("/notify")
public class NotifyController {

     @Autowired
     private NotifyService notifyService;

     @GetMapping("/lineMessage")
     public ResponseEntity<ResponseModel> notifyLineMessage(HttpServletRequest request) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
               result = notifyService.notifyLineMessage(CommonUtil.getMapParameter(request), result);
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }
     


     @GetMapping("/lineMessage/{lineNotifToken}/{message}")
     public ResponseEntity<ResponseModel> notifyLineMessageSimple(HttpServletRequest request,
    		 @PathVariable("lineNotifToken") String lineNotifToken,
    		 @PathVariable("message") String message) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
               result = notifyService.notifyLineMessage(message,lineNotifToken, result);
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }


}