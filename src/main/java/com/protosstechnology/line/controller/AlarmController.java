package com.protosstechnology.line.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.protosstechnology.commons.util.JSONUtil;
import com.protosstechnology.line.ApplicationConstant;
import com.protosstechnology.line.model.ResponseModel;
import com.protosstechnology.line.service.ActivityService;
import com.protosstechnology.line.service.LicenceService;
import com.protosstechnology.line.service.MachineService;
import com.protosstechnology.line.service.NotifyService;
import com.protosstechnology.line.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/alarm")
public class AlarmController {

     @Autowired
     private ActivityService activityService;
     

     @Autowired
     private MachineService machineService;
     

     @Autowired
     private LicenceService licenceService;
     
     @Autowired
     private NotifyService notifyService;
     

     @GetMapping("/notif/{channel}/{action}")
     public ResponseEntity<ResponseModel> notif(HttpServletRequest request,
    		 @PathVariable("channel") String channel,
    		 @PathVariable("action") String action) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers  = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
        	  log.info("-------------- 1");
        	  Map licenceData = (Map) licenceService.find(CommonUtil.getToken(), new ResponseModel()).getData();
        	  List<Map<String,Object>> itemsLicence = (List<Map<String, Object>>) licenceData.get("items");
        	  
        	  String activeStatus   = String.valueOf(itemsLicence.get(0).get("activeStatus"));
        	  String token          = String.valueOf(itemsLicence.get(0).get("token"));
        	  String lineNotifToken = String.valueOf(itemsLicence.get(0).get("lineNotifToken"));
        	  

        	  log.info("-------------- 2");
        	  if(ApplicationConstant.STATUS_ACTIVE.equals(activeStatus)) {
        		  Map machineData = (Map) machineService.find(JSONUtil.mapFromJson("{\"token\":\""+token+"\" , \"channel\":\""+channel+"\" }"), new ResponseModel()).getData();
        		  List<Map<String,Object>> itemsMachine = (List<Map<String, Object>>) machineData.get("items");
        		  
        		  
        		  String logActiveStatus = String.valueOf(itemsMachine.get(0).get("logActiveStatus"));
            	  String lineAlarmStatus = String.valueOf(itemsMachine.get(0).get("lineAlarmStatus"));
            	  String flCode          = String.valueOf(itemsMachine.get(0).get("flCode"));
            	  String activityMessage = String.valueOf(itemsMachine.get(0).get("channelDescription"));
            	  String channelStatus   = String.valueOf(itemsMachine.get(0).get("channelStatus"));
            	  String machineId       = String.valueOf(itemsMachine.get(0).get("#"));
            	  String activityTime    = CommonUtil.getDateTime();

            	  log.info("-------------- 3");
            	  if( "ALARM".equals(action) ) {
                	  log.info("-------------- 4");
            		  if("Y".endsWith(logActiveStatus)) {
                		  activityService.add(JSONUtil.mapFromJson("{\"flCode\":\""+flCode+"\" , \"channel\":\""+channel+"\" , \"activityMessage\":\""+activityMessage+"\"  , \"activityTime\":\""+activityTime+"\" }"), new ResponseModel());
                	  }

                	  log.info("-------------- 5");
                	  if("Y".endsWith(lineAlarmStatus)) {
                		  notifyService.notifyLineMessage(JSONUtil.mapFromJson("{\"message\":\""+activityMessage+"\" ,\"lineNotifToken\":\""+lineNotifToken+"\" }"), new ResponseModel());
                	  }

                	  log.info("-------------- 6");
                	  //itemsMachine.get(0).put("channelStatus", "ALARM");
                	  //machineService.update((Map)itemsMachine.get(0), machineId, new ResponseModel());
            	  } else if(!"ALARM".equals(action) ) {
            		  if("Y".endsWith(logActiveStatus)) {
                		  activityService.add(JSONUtil.mapFromJson("{\"flCode\":\""+flCode+"\" , \"channel\":\""+channel+"\" , \"activityMessage\":\""+activityMessage+"-back to normal\"  , \"activityTime\":\""+activityTime+"\" }"), new ResponseModel());
                	  }
                	  
                	  if("Y".endsWith(lineAlarmStatus)) {
                		  notifyService.notifyLineMessage(JSONUtil.mapFromJson("{\"message\":\""+activityMessage+"-back to normal\" ,\"lineNotifToken\":\""+lineNotifToken+"\" }"), new ResponseModel());
                	  }
            		  
            		  
            		  
            		  //itemsMachine.get(0).put("channelStatus", action);
                	  //machineService.update((Map)itemsMachine.get(0), machineId, new ResponseModel());
            	  }
            	  
            	  

            	  log.info("-------------- 7");
            	  result.setSuccess(true);
            	  result.setMessage("Notify Success");
            	  result.setError_code(null);
            	  result.setData(null);
        	  }
        	  
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }
     
     @GetMapping("/notifTimeShift/{channel}/{action}")
     public ResponseEntity<ResponseModel> notifwebcond(HttpServletRequest request,
    		 @PathVariable("channel") String channel,
    		 @PathVariable("action") String action) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers  = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
        	  
        	  Map licenceData = (Map) licenceService.find(CommonUtil.getToken(), new ResponseModel()).getData();
        	  List<Map<String,Object>> itemsLicence = (List<Map<String, Object>>) licenceData.get("items");
        	  
        	  String activeStatus = String.valueOf(itemsLicence.get(0).get("activeStatus"));
        	  String token        = String.valueOf(itemsLicence.get(0).get("token"));
        	  String lineNotifToken = String.valueOf(itemsLicence.get(0).get("lineNotifToken"));
        	  
        	  
        	  if(ApplicationConstant.STATUS_ACTIVE.equals(activeStatus)) {
        		  Map machineData = (Map) machineService.find(JSONUtil.mapFromJson("{\"token\":\""+token+"\" , \"channel\":\""+channel+"\" }"), new ResponseModel()).getData();
        		  List<Map<String,Object>> itemsMachine = (List<Map<String, Object>>) machineData.get("items");
        		  
        		  
        		  String logActiveStatus = String.valueOf(itemsMachine.get(0).get("logActiveStatus"));
            	  String lineAlarmStatus = String.valueOf(itemsMachine.get(0).get("lineAlarmStatus"));
            	  String flCode          = String.valueOf(itemsMachine.get(0).get("flCode"));
            	  String activityMessage = String.valueOf(itemsMachine.get(0).get("channelDescription"));
            	  String channelStatus   = String.valueOf(itemsMachine.get(0).get("channelStatus"));
            	  String timeShift       = ","+String.valueOf(itemsMachine.get(0).get("timeShift"));
            	  String machineId       = String.valueOf(itemsMachine.get(0).get("#"));
            	  String activityTime    = CommonUtil.getDateTime();
            	  
            	  
            	  if( "ALARM".equals(action) && timeShift.contains(","+CommonUtil.getHour()) ) {
            		  notifyService.notifyLineMessage(JSONUtil.mapFromJson("{\"message\":\""+activityMessage+"\" ,\"lineNotifToken\":\""+lineNotifToken+"\" }"), new ResponseModel());
            	  }
            	  
            	  
            	  
            	  result.setSuccess(true);
            	  result.setMessage("Notify Success");
            	  result.setError_code(null);
            	  result.setData(null);
        	  }
        	  
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }
     
     @GetMapping("/register/{customerCode}")
     public ResponseEntity<ResponseModel> register(HttpServletRequest request,
    		 @PathVariable("customerCode") String customerCode
    		 ) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers  = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
        	  
        	  Map tokenMap    = CommonUtil.getTokenFull();
        	  Map licenceData = (Map) licenceService.find(CommonUtil.getToken(), new ResponseModel()).getData();
        	  log.info("licenceData={}",licenceData);
        	  if((Double)licenceData.get("nitems") == 0.0d) {
        		  licenceData  = (Map) licenceService.add(JSONUtil.mapFromJson("{\"customerCode\":\""+customerCode+"\" , \"macAddress\":\""+tokenMap.get("macAddress")+"\"  , \"token\":\""+tokenMap.get("token")+"\" , \"activeStatus\":\"request\"}"), new ResponseModel()).getData();
        	      log.info("licenceData={}",licenceData);
        	  }
        	  
        	  
        	  
        	  
        	  result.setSuccess(true);
        	  result.setMessage("Register Success");
        	  result.setError_code(null);
        	  result.setData(null);
        	  
        	  
        	  
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }
     
     @GetMapping("/channel/{flCode}/{channel}")
     public ResponseEntity<ResponseModel> channel(HttpServletRequest request,
    		 @PathVariable("flCode") String flCode,
    		 @PathVariable("channel") String channel
    		 ) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers  = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
        	  
        	  Map tokenMap    = CommonUtil.getTokenFull();
        	  Map licenceData = (Map) licenceService.find(CommonUtil.getToken(), new ResponseModel()).getData();
        	  //log.info("licenceData={}",licenceData);
        	  if((Double)licenceData.get("nitems") == 1.0d) {
        		  
        		  Map machineData = (Map) machineService.find(JSONUtil.mapFromJson("{\"flCode\":\""+flCode+"\" , \"token\":\""+tokenMap.get("token")+"\"  ,\"channel\":\""+channel+"\" }"), new ResponseModel()).getData();
        		  
        		  if((Double)machineData.get("nitems") == 0.0d) {
        			  Map<String,Object> mapMachine = new HashMap<String,Object>();
        			  mapMachine.put("flCode", flCode);
        			  mapMachine.put("token", tokenMap.get("token"));
        			  mapMachine.put("channel", channel);
        			  mapMachine.put("logActiveStatus", "N");
        			  mapMachine.put("lineAlarmStatus", "N");
        			  machineService.add(mapMachine, new ResponseModel()).getData();
        		  }
        		  
        		  
        	  }
        	  
        	  
        	  
        	  
        	  
        	  result.setSuccess(true);
        	  result.setMessage("Add Channel Success");
        	  result.setError_code(null);
        	  result.setData(null);
        	  
        	  
        	  
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }

     @GetMapping("/resetbox")
     public ResponseEntity<ResponseModel> resetbox(HttpServletRequest request) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers  = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
        	  
        	  Map tokenMap    = CommonUtil.getTokenFull();
        	  Map licenceData = (Map) licenceService.find(CommonUtil.getToken(), new ResponseModel()).getData();
        	  //log.info("licenceData={}",licenceData);
        	  if((Double)licenceData.get("nitems") == 1.0d) {
        		  
        		  Map machineData = (Map) machineService.find(JSONUtil.mapFromJson("{\"token\":\""+tokenMap.get("token")+"\" }"), new ResponseModel()).getData();
        		  
        		  if((Double)machineData.get("nitems") > 0.0d) {
        			  List<Map<String,Object>> itemsMachine = (List<Map<String, Object>>) machineData.get("items");
        			  for(Map<String,Object> channel:itemsMachine) {
        				  channel.put("channelStatus", "IDLE");
        				  machineService.update(channel, String.valueOf(channel.get("#")), new ResponseModel());
        			  }
        			  
        			  
        		  }
        		  
        		  
        	  }
        	  
        	  
        	  
        	  
        	  
        	  result.setSuccess(true);
        	  result.setMessage("Resetbox Success");
        	  result.setError_code(null);
        	  result.setData(null);
        	  
        	  
        	  
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }



}