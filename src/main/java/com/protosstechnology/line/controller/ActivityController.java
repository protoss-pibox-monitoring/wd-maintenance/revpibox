package com.protosstechnology.line.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.protosstechnology.line.ApplicationConstant;
import com.protosstechnology.line.model.ResponseModel;
import com.protosstechnology.line.service.ActivityService;
import com.protosstechnology.line.util.CommonUtil;

@RestController
@RequestMapping("/activity")
public class ActivityController {

     @Autowired
     private ActivityService activityService;
     

     @GetMapping("/list")
     public ResponseEntity<ResponseModel> list(HttpServletRequest request) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
               /*read request parameterMap to map */
               result = activityService.list(CommonUtil.getMapParameter(request), result);
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }
     

     @GetMapping("/find")
     public ResponseEntity<ResponseModel> find(HttpServletRequest request) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
               /*read request parameterMap to map */
               result = activityService.find(CommonUtil.getMapParameter(request), result);
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }
     

     @GetMapping("/add")
     public ResponseEntity<ResponseModel> add(HttpServletRequest request) {
          /*object response */
          ResponseModel result = new ResponseModel();
          HttpHeaders headers = new HttpHeaders();
          headers.add(ApplicationConstant.CONTENT_TYPE_KEY ,ApplicationConstant.CONTENT_TYPE_JSON);

          try {
               /*read request parameterMap to map */
               result = activityService.add(CommonUtil.getMapParameter(request), result);
               return ResponseEntity.ok().headers(headers).body(result);

          } catch (Exception e) {
               e.printStackTrace();
               return ResponseEntity.ok().headers(headers).body(result);

          }

     }


}