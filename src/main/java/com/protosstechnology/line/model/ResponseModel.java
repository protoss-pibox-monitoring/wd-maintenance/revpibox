package com.protosstechnology.line.model;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.protosstechnology.line.ApplicationConstant;

import lombok.Data;

@Data
public class ResponseModel{

	private Boolean success;
	
	private String message;

	@JsonInclude(Include.NON_NULL)
	private String error_code;

	private Object data;

	public ResponseModel() {
		setSuccess(false);
    	setError_code(ApplicationConstant.ERROR_CODE_1000);
        setMessage("Process Fail");
        setData(new HashMap());
	}

}
